# apple-release-checker

Checks to see if a new beta release is available for the software you specify.

## Setup

Configuration is done entirely through environment variables.

First, set the `RELEASE_CHECKER_OSES` environment variable. It should contain a space-separated list of OSes/software you want to check. Here's an example:

```bash
export RELEASE_CHECKER_OSES="macos ios"
```

(Capitalization does not matter.)

Now, **for each OS/software you specified**, set two variables: `WHATEVER_CURRENT_VERSION` and `WHATEVER_CURRENT_BETA`. Example:

```bash
export MACOS_CURRENT_VERSION=10.15
export MACOS_CURRENT_BETA=2
```

You should be good to go!

## Installation & Usage

```bash
npm i -g apple-release-checker
apple-release-checker
```

If there is a new beta release for the software you specify, you'll get a message like this:

```bash
A new beta release is available: macOS Catalina 10.15 beta 2 (19A487l)
```

Otherwise, there will be no output.

## Debugging/Troubleshooting

```bash
export RELEASE_CHECKER_DEBUG_MODE=true
```

Use the above command to enable debug output.

```bash
unset RELEASE_CHECKER_DEBUG_MODE
```

Use the above command to disable debug output.

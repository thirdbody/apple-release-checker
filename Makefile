npm:
	@npm install

publish: npm
	@echo "//registry.npmjs.org/:_authToken=${NPM_TOKEN}" > ~/.npmrc
	@npm publish

check: npm
	( \
		export RELEASE_CHECKER_OSES="macos"; \
		export MACOS_CURRENT_VERSION=10.15; \
		export MACOS_CURRENT_BETA=1; \
		export RELEASE_CHECKER_TESTING_MODE=true; \
		[[ "$$(node .)" == "A new beta release is available: macOS Catalina 10.15 beta 2 (19A487l)" ]]; \
	)
